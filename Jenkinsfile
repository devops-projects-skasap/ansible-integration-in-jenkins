#!/usr/bin/env groovy

pipeline {
    agent any

    environment{
        ANSIBLE_SERVER = "18.234.227.102"
    }

    stages {

        stage('Copy Files to Ansible Server') {
            steps { 
                script {
                    echo "Copying all necessary files to Ansible control node"
                    sshagent(['AnsibleControlNode']){
                        sh "scp -o StrictHostKeyChecking=no ansible/* ubuntu@${ANSIBLE_SERVER}:/home/ubuntu"
                    
                        withCredentials([sshUserPrivateKey(credentialsId: 'EC2Server', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                            try {
                                sh 'scp $keyfile ubuntu@${ANSIBLE_SERVER}:/home/ubuntu/ssh-key.pem'
                            } catch(err) {
                                echo err.getMessage()
                            }
                        }
                    }
                }   
            }
        }

        stage('Execute Ansible Playbook') {
            steps { 
                script {
                    echo "Calling Ansible playbook to configure EC2 instances"

                    def remote = [:]
                    remote.name = "ansible-server"
                    remote.host = env.ANSIBLE_SERVER
                    remote.allowAnyHosts = true

                     withCredentials([sshUserPrivateKey(credentialsId: 'AnsibleControlNode', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                        remote.user = user
                        remote.identityFile = keyfile

                        sshScript remote: remote, script: "prepare-ansible-server.sh"
                        sshCommand remote: remote, command: "ansible-playbook configure-ec2-servers.yaml"
                     } 
                }   
            }
        }      

    }
}