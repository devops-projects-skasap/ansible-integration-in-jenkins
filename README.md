## Ansible Integration in Jenkins

### Technologies used:

Ansible, Jenkins, AWS, Boto3, Docker, Java, Maven, Linux, Git

### Project Description:

1. Create and configure a dedicated server for Jenkins

2. Create and configure a dedicated server for Ansible Control Node

3. Write Ansible Playbook, which configures 2 EC2 Instances

4. Add SSH key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node servers

5. Configure Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as part of the CI/CD pipeline

6. So the Jenkinsfile configuration will do the following:

    * Connect to the remote Ansible Control Node server,

    * Copy Ansible playbook and configuration files to the remote Ansible Control Node server,

    * Copy the SSH keys for the Ansible Managed Node servers to the Ansible Control Node server,

   * Install Ansible, Python3 and Boto3 on the Ansible Control Node server,

    * With everything installed and copied to the remote Ansible Control Node server, execute the playbook remotely on that Control Node that will configure the 2 EC2 Managed Nodes.

### Instructions:

#### Step 1: Install and configure Jenkins on an AWS EC2 Server:

![image](images/jenkins-1.png)

#### Step 2: Install and configure an Ansible Control Node on another AWS EC2 Server:

![image](images/ansible-control.png)

##### Create `AnsibleControlNode` credential of type `SSH username with private key` in Jenkins (based on the SSH key pair) to enable SSH access to the Ansible Control server from Jenkins:

![image](images/jenkins-credential-1.png)

#### Step 3: Compose a Terraform script to provision 2 EC2 instances on AWS to be managed by Ansible, in addition to configuring a new VPC, creating a subnet with an associated route table, establishing a new internet gateway and creating a security group for the EC2 instances:

```
resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames  = true

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id     = aws_vpc.myapp-vpc.id

  cidr_block = var.subnet_cidr_block

  availability_zone = var.availability_zone

  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id     = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }

}

// Create a new route table and associate it with the subnet
resource "aws_route_table" "myapp-rtb" {
  vpc_id     = aws_vpc.myapp-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet-1.id

  route_table_id = aws_route_table.myapp-rtb.id
}

// Create a new security group
resource "aws_security_group" "myapp-sg" {
  name = "myapp-sg"

  vpc_id     = aws_vpc.myapp-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ansible_server_ip, var.personal_pc_ip]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  
  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-2-image" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = [var.image_name]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

output "myapp-server-ami-id" {
  value = data.aws_ami.latest-amazon-linux-2-image.id
}

resource "aws_key_pair" "myapp-ssh-key" {
  key_name = "${var.env_prefix}-myapp-ssh-key"
  
  public_key = file(var.ssh_public_key)
}


resource "aws_instance" "myapp-server-1" {
  ami = data.aws_ami.latest-amazon-linux-2-image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone = var.availability_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.myapp-ssh-key.key_name

  tags = {
    Name = "${var.env_prefix}-server"
  }
}

resource "aws_instance" "myapp-server-2" {
  ami = data.aws_ami.latest-amazon-linux-2-image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone = var.availability_zone

  associate_public_ip_address = true
  key_name = aws_key_pair.myapp-ssh-key.key_name

  tags = {
    Name = "${var.env_prefix}-server"
  }
}
```

##### Create the EC2 instances by executing:

```
cd terraform

terraform init

terraform apply
```

##### Create `EC2Server` credential of type `SSH username with private key` in Jenkins to enable SSH access to the EC2 servers created above from the Ansible Control server:

![image](images/jenkins-credential-2.png)

#### Step 4: Write a configuration script in Bash to install Ansible, Python3 along with the required Python libraries of `boto3` and `botocore` on the Ansible Control server:

```
#!/bin/bash

sudo apt-add-repository ppa:ansible/ansible -y
sudo apt update

sudo apt install ansible -y

sudo apt install python3-pip -y

pip3 install boto3 botocore
```

#### Step 5: Write an Ansible playbook `configure-ec2-servers.yaml` to configure the EC2 servers by installing Docker, Docker Compose tool and the relevant Python libraries and adding `ec2-user` user to the `docker` group:

```
- name: Configure EC2 server
  hosts: aws_ec2
  become: yes
  become_user: root
  tasks:
    - name: Install Python3
      ansible.builtin.yum:
        name: "{{ packages }}"
        state: latest
        update_cache: yes
      vars:
        ansible_python_interpreter: /usr/bin/python
        packages:
          - python3
          - python3-pip

    - name: Update the package cache
      ansible.builtin.yum:
        name: "*"
        state: latest
        update_cache: yes
      ignore_errors: yes

    - name: Install Docker
      ansible.builtin.yum:
        name: "{{ packages }}"
        state: latest
        update_cache: yes
      vars:
        packages:
          - docker

    - name: Start and enable Docker
      ansible.builtin.service:
        name: docker
        state: started
        enabled: yes

    - name: Install Docker Compose
      ansible.builtin.get_url:
        url: https://github.com/docker/compose/releases/download/1.29.2/docker-compose-{{ lookup('pipe', 'uname -s') }}-{{ lookup('pipe', 'uname -m') }}
        dest: /usr/local/bin/docker-compose
        mode: "a+x"

    - name: Install docker and docker-compose Python packages
      ansible.builtin.pip:
        name:
          - docker
          - docker-compose

- name: Configure users on EC2 server
  hosts: aws_ec2
  become: yes
  become_user: root
  tasks:
    - name: Add 'ec2-user' to 'docker' group
      ansible.builtin.user:
        user: ec2-user
        groups: docker
        append: yes

    - name: ec2-user reconnect to server session
      ansible.builtin.meta: reset_connection
```

#### Step 6: Compose a Jenkinsfile to copy the Ansible playbook, configuration files and the SSH keys for the Ansible Managed EC2 servers to the Ansible Control server in the first stage of the CI/CD pipeline, and then execute the playbook remotely on the Control server that will configure the 2 EC2 server nodes within the second stage after executing the above Bash script on the Ansible server to initialize it:

```
#!/usr/bin/env groovy

pipeline {
    agent any
 
    environment{
        ANSIBLE_SERVER = "18.234.227.102"
    }

    stages {

        stage('Copy Files to Ansible Server') {
            steps { 
                script {
                    echo "Copying all necessary files to Ansible control node"
                    sshagent(['AnsibleControlNode']){
                        sh "scp -o StrictHostKeyChecking=no ansible/* ubuntu@${ANSIBLE_SERVER}:/home/ubuntu"
                    
                        withCredentials([sshUserPrivateKey(credentialsId: 'EC2Server', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                            try {
                                sh 'scp $keyfile ubuntu@${ANSIBLE_SERVER}:/home/ubuntu/ssh-key.pem'
                            } catch(err) {
                                echo err.getMessage()
                            }
                        }
                    }
                }   
            }
        }

        stage('Execute Ansible Playbook') {
            steps { 
                script {
                    echo "Calling Ansible playbook to configure EC2 instances"

                    def remote = [:]
                    remote.name = "ansible-server"
                    remote.host = env.ANSIBLE_SERVER
                    remote.allowAnyHosts = true

                     withCredentials([sshUserPrivateKey(credentialsId: 'AnsibleControlNode', keyFileVariable: 'keyfile', usernameVariable: 'user')]) {
                        remote.user = user
                        remote.identityFile = keyfile

                        sshScript remote: remote, script: "prepare-ansible-server.sh"
                        sshCommand remote: remote, command: "ansible-playbook configure-ec2-servers.yaml"
                     } 
                }   
            }
        }      

    }
}
```

##### Jenkins CI/CD pipeline operation:

![image](images/jenkins-2.png)

##### First stage of the pipeline starting:

![image](images/jenkins-3.png)

##### Second stage of the pipeline starting:

![image](images/jenkins-4.png)

##### Execution of the Ansible playbook within the second stage:

![image](images/ansible-play-1.png)

![image](images/ansible-play-2.png)