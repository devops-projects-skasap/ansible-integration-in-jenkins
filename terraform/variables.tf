variable "region" {
  type        = string
  description = "AWS region"
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_cidr_block" {
  type        = string
  description = "Subnet CIDR Block"
  default = "10.0.10.0/24"
}

variable availability_zone {
  type = string
  description = "Availability Zone"
}

variable env_prefix {
  type        = string
  description = "Environment Prefix"
}

variable ansible_server_ip {
  type        = string
  description = "Ansible Server IP Address"
}

variable personal_pc_ip {
  type        = string
  description = "Personal PC IP Address"
}

variable instance_type {
  type        = string
  description = "Instance Type"
}

variable ssh_public_key {
  type        = string
  description = "Public SSH Key"
}

variable ssh_private_key {
  type        = string
  description = "Private SSH Key"
}

variable image_name {
  type        = string
  description = "Image Name"
}